# PEM-360: A dataset of 360° videos with continuous Physiological measurements, subjective Emotional ratings and Motion traces

## Citing this work

```
@inproceedings{10.1145/3524273.3532895,
author = {Guimard, Quentin and Robert, Florent and Bauce, Camille and Ducreux, Aldric and Sassatelli, Lucile and Wu, Hui-Yin and Winckler, Marco and Gros, Auriane},
title = {PEM360: A Dataset of 360° Videos with Continuous Physiological Measurements, Subjective Emotional Ratings and Motion Traces},
year = {2022},
isbn = {9781450392839},
publisher = {Association for Computing Machinery},
address = {New York, NY, USA},
url = {https://doi.org/10.1145/3524273.3532895},
doi = {10.1145/3524273.3532895},
booktitle = {Proceedings of the 13th ACM Multimedia Systems Conference},
pages = {252–258},
numpages = {7},
keywords = {user experiment, physiological data, emotions, 360° videos},
location = {Athlone, Ireland},
series = {MMSys '22}
}
```

## Authors

- [Quentin Guimard](mailto:quentin.guimard@univ-cotedazur.fr)* - Université Côte d'Azur, CNRS, I3S
- [Florent Robert](mailto:florent.robert@inria.fr)* - Université Côte d'Azur, CNRS, Inria, I3S
- Camille Bauce - Université Côte d'Azur, CNRS, I3S
- Aldric Ducreux - Université Côte d'Azur, CNRS, I3S
- Lucile Sassatelli - Université Côte d'Azur, CNRS, I3S, Institut Universitaire de France
- Hui-Win Yu - Université Côte d'Azur, Inria
- Marco Winckler - Université Côte d'Azur, CNRS, Inria, I3S
- Auriane Gros - Université Côte d'Azur, CHU de Nice, CoBTek

*equal contribution
## Additional resources (heavy files)

HDF5 pre-computed saliency maps, as well as the video files that were used in the experiment can be found [here][1].

## Raw data

The folder `./raw_data/` contains the unprocessed, raw data from the experiment. For each user `<user_id>` (from 01 to 34), there is a folder named `user_<user_id>/`. Inside this directory, there is:
- a single file called `shimmer.csv` containing continuous physiological measurements for the entirety of the experiment, from the first to the last video, including the breaks between the videos,
- a file `video_<video_id>.csv` for each video `<video_id>` the user has watched while in VR, containing continuous head and gaze positions.

## Requirements

Requirements to run all the notebooks can be found in `./requirements.txt`. Separate requirements files were made for the Python scripts allowing to compute the content-based (HL and LL) saliency maps and the emotional maps.

You can install the requirements for the notebooks by running `python3 -m pip install -r ./requirements.txt`.

## Resampling the data

In order to make the analysis in the main notebook `./notebook.ipynb`, we have decided to resample all the data to 100 Hz using linear interpolation.

You can resample the data by running the notebook `./resample_data.ipynb`. The resampled data will be stored in `./resampled_data/`.

## Running the notebook

The main notebook contaning the analysis with figures can be found under `./notebook.ipynb`.

Once you have installed all the required libraries, resampled the data, and downloaded or computed the saliency maps, the execution should be pretty straight forward.

The first time the notebook is executed, all the data in `./resampled_data/` is parsed, processed and stored in a Pandas DataFrame, which is then written as a Pickle file to the disk under `./resampled_data/model_data.pkl`. Running the notebook afterwards directly loads the DataFrame instead of parsing and processing all the data again.

Computing the NSS can also take a long time (~4-5 hours on a laptop equipped with an Intel® Core™ i7-10875H CPU and 64GB of RAM). The first time you run the notebook, a Pickle file containing the Pandas DataFrame is written to the disk under `./NSS_150.pkl`. Once the NSS in stored, you should skip the notebook cells where it is computed and directly load the file in the cell below.

## Saliency maps

The pre-computed saliency maps can be downloaded from [here][1].

### Ground-truth saliency (based on user gaze)

If you wish to compute the ground-truth saliency maps manually, please follow the next steps:

#### Requirements

The requirements are the same as in `./requirements.txt`. You can install the required libraries by running `python3 -m pip install -r ./requirements.txt`.

#### Running the notebook

In order to compute the ground-truth saliency maps, you need to run the notebook `./saliency_maps/compute_gt_saliency.ipynb`.

The first time the notebook is executed, all the data in `./resampled_data/` is parsed, processed and stored in a Pandas DataFrame, which is then written as a Pickle file to the disk under `./resampled_data/model_data.pkl`. Running the notebook afterwards directly loads the DataFrame instead of parsing and processing all the data again.

The ground-truth saliency maps are stored in a single HDF5 file (`./saliency_maps/gt_saliency.h5`) of shape `(N, 216, 384)`, where `N` is the length of the dataframe mentioned above.

### Content-based saliency (low-level and high-level)

If you wish to compute the HL and LL saliency maps manually, please follow the next steps:

#### Requirements

- You must download the videos from [here][1] and place them in the (newly created) `./videos/` folder.
- The required Python libraries can be found in `./saliency_maps/requirements.txt` and installed using `python3 -m pip install -r ./saliency_maps/requirements.txt`. The versions specified in this file have been tested to work with our code, but other versions may work.
- To compute the high-level saliency, you need the weights and classes from [here][2] for YOLOv4 object detector to work properly.
- Be aware that you need a GPU with enough VRAM (approx. 8 GB needed) to compute the HL saliency maps.
- You need to create the appropriate destination folders, namely `./saliency_maps/low-level/` and `./saliency_maps/high-level/` for LL and HL saliency, respectively.

#### Running the scripts

- Scripts need to be run from `./saliency_maps/`
- You can compute low-level saliency maps using `python ./compute_ll_saliency.py <video_id>`, where `<video_id>` corresponds to the video ID (12, 13, 17, 23, 27, 32, or 73).
- You can compute high-level saliency maps using `python ./compute_hl_saliency.py <video_id>`, where `<video_id>` corresponds to the video ID (12, 13, 17, 23, 27, 32, or 73).
- The resulting saliency maps will be saved in the folders described in "Requirements". There is one HDF5 file per video of shape `(n_frames, 216, 384)`.

## Emotional maps

You can compute emotional maps as described in section 4.4 of the paper using the script in `./emotional_maps/`.

### Requirements

- You must download the videos from [here][1] and place them in the (newly created) `./videos/` folder.
- The required Python libraries can be found in `./emotional_maps/requirements.txt` and installed using `python3 -m pip install -r ./emotional_maps/requirements.txt`. The versions specified in this file have been tested to work with our code, but other versions may work.
- You need to create the appropriate destination folder, namely `./emotional_maps/generated/`.

### Running the script

- Scripts need to be run from `./emotional_maps/`
- You can compute low-level saliency maps using `python ./compute_emotional_map.py -s ../raw_data/user_<user_id>/shimmer.csv -r ../raw_data/user_<user_id>/video_<video_id>.csv -v ../videos/video_<video_id>.webm`, where `<user_id>` corresponds to the user ID (from 01 to 34) and `<video_id>` corresponds to the video ID (12, 13, 17, 23, 27, 32, or 73).

#### Detailed script parameters

A simple example on how to run the script with basic arguments is given above, but `compute_emotional_map.py` has a lot of parameters you can play with:
- `-h, --help`: see all available parameters and their description
- Required parameters:
	- `-s <path/to/file.csv>, --shimmer <path/to/file.csv>`: path to CSV Shimmer recordings
	- `-r <path/to/file.csv>, --vr <path/to/file.csv>`: path to CSV head motion/gaze VR recordings
	- `-v <path/to/video.webm>, --video <path/to/video.webm>`: path to WebM video file
- Optional parameters:
	- Processing:
		- `--batch`: process only a batch from the first frame `-ff` to last frame `-lf`, if not present, process the whole video by default
		- `-ff <frame_id>, --firstf <frame_id>`: start computing the emotional map at frame `<frame_id>` (indexing starts at 1)
		- `-lf <frame_id>, --lastf <frame_id>`: stop computing the emotional map at frame `<frame_id>`
	- Output:
		- `-o <path/to/output.mp4>, --output <path/to/output.mp4>`: path to emotional map video output
	- Modifiers:
		- `-c <opencv_colormap>, --color <opencv_colormap>`: OpenCV colormap to be used for user arousal
		- `-t <window>, --tailtime <window>`: window size to compute user arousal and position, in seconds (defaults to 1)
		- `-pt <size>, --pointsize <size>`: radius of the user's gaze shown on the emotional map, in pixels (defaults to 5)
		- `-bp <proportion>, --blendprop <proportion>`: opacity of the emotional map compared to the video (defaults to 0.7)

## License

All the data provided in `./raw_data/` is licensed under the [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International Public License][3] (CC BY-NC-ND 4.0).

The code provided in this repository is licensed under the [GNU General Public License (GPL) v3.0][4].

Part of the provided code is from other projects and repositories:
- `./saliency_maps/nfov.py` is from the [Equirectangular-toolbox][5], licensed under the Apache 2.0 License, and has been slightly modified.
- `./saliency_maps/pySaliencyMap.py` and `./saliency_maps/pySaliencyMapDefs.py` are from [pySaliencyMap][6], licensed under the MIT License, and have not been modified.

[1]: https://unice-my.sharepoint.com/:f:/g/personal/quentin_guimard_unice_fr/EtZKn-eWuRFOoki8jlmMvg0BTdw-hXOhNf8mzlbHoKKYkQ?e=QNMdmd
[2]: https://wiki.loliot.net/docs/lang/python/libraries/yolov4/python-yolov4-about/
[3]: https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
[4]: https://www.gnu.org/licenses/gpl-3.0.en.html
[5]: https://github.com/NitishMutha/equirectangular-toolbox/blob/master/nfov.py
[6]: https://github.com/akisatok/pySaliencyMap
