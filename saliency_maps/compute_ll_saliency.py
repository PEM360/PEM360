 # 
 # This file is part of the PEM360 distribution (https://gitlab.com/pem360/PEM360).
 # Copyright (c) 2022 Quentin Guimard.
 # 
 # This program is free software: you can redistribute it and/or modify  
 # it under the terms of the GNU General Public License as published by  
 # the Free Software Foundation, version 3.
 #
 # This program is distributed in the hope that it will be useful, but 
 # WITHOUT ANY WARRANTY; without even the implied warranty of 
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 # General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License 
 # along with this program. If not, see <http://www.gnu.org/licenses/>.
 #

import sys
import cv2 as cv
import numpy as np
import os
import tables
from skimage.measure import block_reduce
from nfov import NFOV
import pySaliencyMap

VIDEO_FOLDER = '../videos/'

video_id = sys.argv[1]


def backproject_to_equirectangular(equir_frame_, screen_cords_, values):
    uf = np.mod(screen_cords_.T[0], 1) * equir_frame_.shape[1]  # long - width
    vf = np.mod(screen_cords_.T[1], 1) * equir_frame_.shape[0]  # lat - height

    x0 = np.floor(uf).astype(int)  # coord of pixel
    y0 = np.floor(vf).astype(int)

    base_y0 = np.multiply(y0, equir_frame_.shape[1])
    A_idx = np.add(base_y0, x0)

    flat_img = np.copy(np.reshape(equir_frame_, [-1, equir_frame_.shape[2]]))
    flat_img[A_idx] += np.reshape(values, [-1, values.shape[2]])

    flat_img = np.reshape(flat_img, equir_frame_.shape)
    return flat_img


def normalize_eulerian(longitude, latitude):
    norm_longitude = 1 - (longitude / (2 * np.pi))
    norm_latitude = latitude / np.pi
    return norm_longitude, norm_latitude


def sample_points():
    sampling_points = []
    GOLDEN_RATIO = (1 + np.sqrt(5)) / 2

    for i in range(-50, 50 + 1):
        longitude = (2 * np.pi * i * (1 / GOLDEN_RATIO)) % (2 * np.pi)
        latitude = (np.arcsin((2 * i) / (2 * 50 + 1))) + (np.pi / 2)
        norm_long, norm_lat = normalize_eulerian(longitude, latitude)
        sampling_points.append([norm_long, norm_lat])
    sampling_points = np.array(sampling_points)
    return sampling_points


file = os.path.join(VIDEO_FOLDER, f'video_{video_id}.webm')
# First pass to get properties
vidcap = cv.VideoCapture(file)
frame_id = 1
ret, frame = vidcap.read()
while vidcap.isOpened() and ret:
    ret, _ = vidcap.read()
    if not ret:
        break
    frame_id += 1
vidcap.release()
cv.destroyAllWindows()

HEIGHT = frame.shape[0]
WIDTH = frame.shape[1]
LENGTH = frame_id

sampling_points = sample_points()

extractors = []
for _ in sampling_points:
    extractors.append(pySaliencyMap.pySaliencyMap(512, 512))

nfov = NFOV(512, 512)

output = f'./low-level/video_{video_id}.h5'
shape = (LENGTH, HEIGHT // 5, WIDTH // 5)
atom = tables.UInt8Atom()
filters = tables.Filters(complevel=9, complib='bzip2')
h5f = tables.open_file(output, 'w')
ca = h5f.create_carray(h5f.root, 'carray', atom, shape,
                       filters=filters)

# Compute saliency maps online
vidcap = cv.VideoCapture(file)
frame_id = 0
while vidcap.isOpened():
    ret, frame = vidcap.read()
    if not ret:
        break

    print(video_id, frame_id, '/', LENGTH)

    backprojected_saliency = np.zeros(frame.shape)
    tile_per_pixel = np.zeros(frame.shape)

    for pt_id, center_point in enumerate(sampling_points):
        screen_coords, fov_frame = nfov.toNFOV(frame, center_point)

        local_saliency = np.stack((extractors[pt_id].SMGetSM(fov_frame),) * 3, axis=-1)
        tile_pixels = np.ones(fov_frame.shape)

        backprojected_saliency = backproject_to_equirectangular(backprojected_saliency, screen_coords, local_saliency)
        tile_per_pixel = backproject_to_equirectangular(tile_per_pixel, screen_coords, tile_pixels)

    backprojected_saliency = backprojected_saliency[:, :, 0] / np.clip(tile_per_pixel[:, :, 0], a_min=1, a_max=None)
    ca[frame_id, :, :] = block_reduce((backprojected_saliency * 255).astype(np.uint8), block_size=(5, 5), func=np.mean)

    frame_id += 1
vidcap.release()
cv.destroyAllWindows()
h5f.close()
