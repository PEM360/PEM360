import argparse
import math
import os.path as path

import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tqdm.auto import tqdm

import utils_shimmer as shimmer

FIXATIONS_DISPERTION_THRESHOLD = 1.0  # (°)
FIXATIONS_DURATION_THRESHOLD = 0.150  # (s)

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)
pd.set_option('mode.chained_assignment', None)  # Remove SettingWithCopyWarning, remove for debugging


def get_video_info(video_path):
    video = cv2.VideoCapture(video_path)
    ret, frame = video.read()

    fps = video.get(cv2.CAP_PROP_FPS)
    frame_count = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    duration = frame_count / fps

    return frame, fps, duration, frame_count, video


def cartesian_to_equir(x, y, z, equir_w, equir_h):
    theta = np.arctan2(z, x)
    phi = np.arccos(y)

    long = theta * 180 / math.pi
    lat = phi * 180 / math.pi

    x_equir = ((long + 180) / 360) * equir_w
    y_equir = (lat / 180) * equir_h

    data = {'x': x_equir, 'y': y_equir}
    df = pd.DataFrame(data)
    df['x'] = df['x'].astype(int)
    df['y'] = df['y'].astype(int)

    return df


def add_equir_coords(record, image):
    image_W = image.shape[1]
    image_H = image.shape[0]

    gaze_equir = cartesian_to_equir(record['gaze_x'], record['gaze_y'], record['gaze_z'], image_W, image_H)
    record['equir_gaze_x'] = gaze_equir['x']
    record['equir_gaze_y'] = gaze_equir['y']

    head_equir = cartesian_to_equir(record['head_x'], record['head_y'], record['head_z'], image_W, image_H)
    record['equir_head_x'] = head_equir['x']
    record['equir_head_y'] = head_equir['y']

    return record


def read_record(path):
    csv = pd.read_csv(path)
    ## FIX KEYERROR FRAME 0
    if not csv[csv['frame'] == 0].empty:
        csv['frame'] = csv['frame'] + 1
    return csv


def get_equir_gaze_by_frame(record):
    equir_gaze_frame = pd.DataFrame(
        record.groupby('frame').apply(lambda x: [int(x['equir_gaze_x'].mean()), int(x['equir_gaze_y'].mean())]))
    equir_gaze_frame.reset_index(level=equir_gaze_frame.index.names, inplace=True)
    equir_gaze_frame['equir_coords'] = equir_gaze_frame.iloc[:, [1]]

    return equir_gaze_frame


# FROM: https://stackoverflow.com/questions/29731726/how-to-calculate-a-gaussian-kernel-matrix-efficiently-in-numpy
# return kernel in a matrix the size of image_size. max=1, min=0
def gaussian_heatmap(center=(2, 2), image_size=(10, 10), sig=1):
    """
    It produces single gaussian at expected center
    :param center:  the mean position (X, Y) - where high value expected
    :param image_size: The total image size (width, height)
    :param sig: The sigma value
    :return:
    """
    x_axis = np.linspace(0, image_size[0] - 1, image_size[0]) - center[0]
    y_axis = np.linspace(0, image_size[1] - 1, image_size[1]) - center[1]
    xx, yy = np.meshgrid(x_axis, y_axis)
    kernel = np.exp(-0.5 * (np.square(xx) + np.square(yy)) / np.square(sig))

    return kernel


def get_gaussian_SCL(heatmap_mat, SCL):
    SCL_mat = heatmap_mat.copy()
    SCL_mat = SCL_mat * SCL

    return SCL_mat


# return the sum of the nth last heatmap matrices from frame frame
def cum_heatmap(frame):
    first = frame - n
    has_first = False if first < batch_first_frame else True

    heatmap_cum = data[frame - 1]['heatmap_cum_mat'].copy()
    heatmap_cum += data[frame]['heatmap_mat']
    if has_first:
        heatmap_cum -= data[first]['heatmap_mat']
        del data[first]['heatmap_mat']

    return heatmap_cum


def cum_SCL(frame):
    SCL_cum = data[frame - 1]['SCL_cum_mat'].copy()

    # Get vertical and horizontal index of max value (center of point)
    max_vi, max_hi = np.unravel_index(data[frame]['SCL_mat'].argmax(), data[frame]['SCL_mat'].shape)

    data[frame]['hfirst'] = max_hi - SCL_point_size if max_hi - SCL_point_size >= 0 else 0
    data[frame]['hlast'] = max_hi + SCL_point_size if max_hi + SCL_point_size <= image_W else image_W
    data[frame]['vfirst'] = max_vi - SCL_point_size if max_vi - SCL_point_size >= 0 else 0
    data[frame]['vlast'] = max_vi + SCL_point_size if max_vi + SCL_point_size <= image_H else image_H

    # SCL_mat to max intensity
    ma = np.amax(data[frame]['SCL_mat'])
    data[frame]['SCL_mat'] = np.full(data[frame]['SCL_mat'].shape, ma)

    # insert SCL point into the cumulative mat
    SCL_cum[data[frame]['vfirst']:data[frame]['vlast'], data[frame]['hfirst']:data[frame]['hlast']] = \
        data[frame]['SCL_mat'][data[frame]['vfirst']:data[frame]['vlast'], data[frame]['hfirst']:data[frame]['hlast']]

    del data[frame]['SCL_mat']

    return SCL_cum


def get_mask_from_mat(mat):
    gray_mask = cv2.normalize(mat, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    gray_mask = cv2.applyColorMap(gray_mask, cv2.COLORMAP_BONE)
    gray_mask = cv2.normalize(gray_mask, gray_mask, 0., 1., cv2.NORM_MINMAX, cv2.CV_32F)

    return gray_mask


def debug_mat(mat):
    print('f: ' + str(f) + ' | min: ' + str(np.amin(mat)) + ' | max: ' + str(np.amax(mat)))


def debug_img(img):
    plt.imshow(img)
    plt.show()


def get_color_from_mat(mat):
    # Normalise cumulative SCL mat from 0 to 255
    SCL_mat = (mat * 255) / SCL_max
    SCL_mat[SCL_mat == 0] = 255

    SCL_mat_img = cv2.normalize(SCL_mat, None, alpha=np.amin(SCL_mat), beta=np.amax(SCL_mat), norm_type=cv2.NORM_MINMAX,
                                dtype=cv2.CV_8U)
    SCL_mat_img = cv2.applyColorMap(SCL_mat_img, COLORMAP_AROUSAL)

    SCL_mat_img = np.float32(SCL_mat_img)
    return SCL_mat_img


############################################################################################################
############################	ARGV Commands	############################################################

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser(description='Generate cummulative heatmap')

group_path_required = ap.add_argument_group('required path arguments')
group_path_optional = ap.add_argument_group('optional path arguments')
group_processing = ap.add_argument_group('processing arguments')
group_processing_optional = ap.add_argument_group('optional processing arguments')
group_optional_heatmap = ap.add_argument_group('optional heatmap modifier arguments')

group_optional_heatmap.add_argument("-c", "--color", required=False, default='COLORMAP_JET',
                                    choices=['COLORMAP_AUTUMN', 'COLORMAP_BONE', 'COLORMAP_CIVIDIS', 'COLORMAP_COOL',
                                             'COLORMAP_DEEPGREEN',
                                             'COLORMAP_HOT', 'COLORMAP_HSV', 'COLORMAP_INFERNO', 'COLORMAP_JET',
                                             'COLORMAP_MAGMA', 'COLORMAP_OCEAN',
                                             'COLORMAP_PARULA', 'COLORMAP_PINK', 'COLORMAP_PLASMA', 'COLORMAP_RAINBOW',
                                             'COLORMAP_SPRING',
                                             'COLORMAP_SUMMER', 'COLORMAP_TURBO', 'COLORMAP_TWILIGHT'],
                                    help="Open cv color map to use (default COLORMAP_JET)")
group_optional_heatmap.add_argument("-t", "--tailtime", required=False, default=1, type=int,
                                    help="Seconds taken into account for each heatmap sums (default 1). \
                        for exemple for t=5 data from the last 5 seconds will be shown on the heatmap.")
group_optional_heatmap.add_argument("-pt", "--pointsize", required=False, default=5, type=int,
                                    help="Point size radius in pixel for the heatmap (default 5)")
group_optional_heatmap.add_argument("-bp", "--blendprop", required=False, default=0.70, type=float,
                                    help="Blend proportion or weight of the heatmap compared to the video \
                                    (default 0.70). bp=1 => Only heatmap | bp=0 => Only video.")

group_path_required.add_argument("-s", "--shimmer", required=True,
                                 help="Path to shimmer record data csv")
group_path_required.add_argument("-r", "--vr", required=True,
                                 help="Path to vr record data csv")
group_path_required.add_argument("-v", "--video", required=True,
                                 help="Path to video webm")
group_path_optional.add_argument("-o", "--output", required=False, default='./generated/',
                                 help="Heatmap output directory path (default: ./generated/)")

group_processing_optional.add_argument("-ff", "--firstf", required=False, default=0, type=int,
                                       help="Start the heatmap at this frame (first frame is 1)")
group_processing_optional.add_argument("-lf", "--lastf", required=False, default=0, type=int,
                                       help="Stop the heatmap at this frame")
group_processing.add_argument("--batch", help="Process only a batch from first frame -ff to last frame -lf. \
                            If not present process the whole video by default", default=False, action="store_true")

args = vars(ap.parse_args())

COLORMAP_CORS = {'COLORMAP_AUTUMN': 0,
                 'COLORMAP_BONE': 1,
                 'COLORMAP_CIVIDIS': 17,
                 'COLORMAP_COOL': 8,
                 'COLORMAP_DEEPGREEN': 21,
                 'COLORMAP_HOT': 11,
                 'COLORMAP_HSV': 9,
                 'COLORMAP_INFERNO': 14,
                 'COLORMAP_JET': 2,
                 'COLORMAP_MAGMA': 13,
                 'COLORMAP_OCEAN': 5,
                 'COLORMAP_PARULA': 12,
                 'COLORMAP_PINK': 10,
                 'COLORMAP_PLASMA': 15,
                 'COLORMAP_RAINBOW': 4,
                 'COLORMAP_SPRING': 7,
                 'COLORMAP_SUMMER': 6,
                 'COLORMAP_TURBO': 20,
                 'COLORMAP_TWILIGHT': 18}
COLORMAP_AROUSAL = COLORMAP_CORS[args["color"]]

SHIMMER_DATA_PATH = args["shimmer"]
RECORD_PATH = args["vr"]
VIDEO_PATH = args["video"]
OUTPUT = args["output"] + path.basename(RECORD_PATH).split('.')[0] + '.mp4'

point_size = args["pointsize"]
SCL_point_size = point_size * 2
t = args["tailtime"]
blend_proportion = args["blendprop"]

batch_first_frame = args["firstf"]
batch_last_frame = args["lastf"]
whole_video = not args["batch"]  # automatically get the batch first and last frame to process the whole video

if __name__ == '__main__':
    # Get metadata from video
    image, fps, video_duration, frame_count, video_cap = get_video_info(VIDEO_PATH)
    image_W = image.shape[1]
    image_H = image.shape[0]

    if whole_video:
        batch_first_frame = 1
        batch_last_frame = frame_count

    # Read record
    print("Reading record " + RECORD_PATH)
    record = read_record(RECORD_PATH)

    # Read Shimmer's data
    print("Reading shimmer data " + SHIMMER_DATA_PATH)
    shimmer_data = shimmer.per_frame_data(SHIMMER_DATA_PATH, fps, record.iloc[0]['time'], video_duration * 1000)

    SCL_min, SCL_max = math.inf, -math.inf
    for shimmer_values in shimmer_data.values():
        SCL_min = shimmer_values['SCL'] if shimmer_values['SCL'] < SCL_min else SCL_min
        SCL_max = shimmer_values['SCL'] if shimmer_values['SCL'] > SCL_max else SCL_max

    # Quick fix Shimmer last frame missing
    if max(shimmer_data.keys()) < batch_last_frame:
        print("Skipping frames " + str(max(shimmer_data.keys())) + " to " + str(batch_last_frame) +
              " due to missing last frames in Shimmers data!")
        batch_last_frame = max(shimmer_data.keys())

    # Process coordinates and get avg coords of every frame
    add_equir_coords(record, image)
    equir_gaze_frame = get_equir_gaze_by_frame(record)
    equir_gaze_frame = equir_gaze_frame[equir_gaze_frame['frame'] <= batch_last_frame]

    # Add missing frames
    for frame in range(batch_first_frame, batch_last_frame + 1):
        if equir_gaze_frame[equir_gaze_frame['frame'] == frame].empty:
            prev_line = equir_gaze_frame[equir_gaze_frame['frame'] == frame - 1].reset_index(drop=True).to_dict()
            line = {frame - 1: {"frame": frame, 0: prev_line[0][0], 'equir_coords': prev_line['equir_coords'][0]}}
            equir_gaze_frame = pd.concat(
                [equir_gaze_frame.iloc[:frame - 1], pd.DataFrame.from_dict(line, orient='index'),
                 equir_gaze_frame.iloc[frame - 1:]]).reset_index(drop=True)

    # Generate the data dictionary (frame_id: {stuff})
    data = {}
    for index, row in equir_gaze_frame.iterrows():
        data[row['frame']] = {'equir_coords': row['equir_coords'],
                              'SCL': shimmer_data[row['frame']]['SCL']}

    # Free memory before starting heatmaps processing
    del equir_gaze_frame
    del record
    del shimmer_data

    # Heatmap

    # Create the video to store the resulting processed heatmaps
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    video = cv2.VideoWriter(OUTPUT, fourcc, fps, (image_W, image_H))

    print("Processing frame " + str(batch_first_frame) + " to " + str(batch_last_frame))
    for f in tqdm(range(batch_first_frame, batch_last_frame + 1)):
        if f not in data.keys():
            print("There is no record for frame " + str(f) + "! Replaced by a copy of the previous frame... WIP")
            # TODO: copy last img, heatmap_mat, and heatmap_cum ; add to video too
            continue

        # Get the gaussian position heatmap
        data[f]['heatmap_mat'] = gaussian_heatmap(data[f]['equir_coords'], (image_W, image_H), point_size)
        data[f]['SCL_mat'] = get_gaussian_SCL(data[f]['heatmap_mat'], data[f]['SCL'])

        # Sum heatmaps of previous n(t) frames for each frames
        n = int(fps) * t  # number of frame taken into account for each heatmap sums
        if f == batch_first_frame:
            data[f]['heatmap_cum_mat'] = data[f]['heatmap_mat']
            data[f]['SCL_cum_mat'] = np.full(data[f]['SCL_mat'].shape, 0)
        else:
            data[f]['heatmap_cum_mat'] = cum_heatmap(f)
            data[f]['SCL_cum_mat'] = cum_SCL(f)
            del data[f - 1]['heatmap_cum_mat']
            del data[f - 1]['SCL_cum_mat']

        # Get the corresponding mask and color image from matrix
        mask_img = get_mask_from_mat(data[f]['heatmap_cum_mat'])
        color_img = get_color_from_mat(data[f]['SCL_cum_mat'])

        # Blend arousal (SCL) and position heatmap together to make a 2D colormap
        blended_img = cv2.multiply(color_img, mask_img)
        blended_img = np.uint8(blended_img)

        # Blend with video frame
        video_cap.set(cv2.CAP_PROP_POS_FRAMES, f)
        ret, frame_cap = video_cap.read()
        if not ret:
            print("Error when trying to read frame " + str(f) + " of video. Actual video is shorter than VR records.")
            break
        blended_img = cv2.addWeighted(blended_img, blend_proportion, frame_cap, 1 - blend_proportion, 0)

        # Save result image into the video
        video.write(blended_img)

    cv2.destroyAllWindows()
    video.release()
    print("Video saved in: " + OUTPUT)
