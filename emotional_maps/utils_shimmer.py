import csv

def get_data(data_path, video_start, video_duration_ms):
    data_csv = open(data_path, newline='')
    data = {'time': [], 'PPG': [], 'SCL': []}

    # Get data from csv
    # CSV HEADER -> time,range,conductance,resistance,ppg,
    i = 0
    for row in data_csv:
        newrow = row.split(',')
        if i > 2:  # skip csv header
            floatTimestamp = float(newrow[0])
            if floatTimestamp >= video_start:
                if (video_duration_ms + video_start) >= floatTimestamp:
                    data['time'].append((floatTimestamp - video_start) / 1000)
                    data['SCL'].append(float(newrow[2]))
                    data['PPG'].append(float(newrow[4]))
        i += 1

    return data


def per_frame_data(data_path, fps, video_start, video_duration_ms):
    frames_data = {}
    data = get_data(data_path, video_start, video_duration_ms)

    times, SCLs, PPGs = data['time'], data['SCL'], data['PPG']

    frame_end_time = 1/fps
    j = 0  # frame start i
    f = 1  # frame id / number
    for i in range(0, len(times)):
        if times[i] < frame_end_time:
            continue

        SCL = sum(SCLs[j:i]) / len(SCLs[j:i])
        PPG = sum(PPGs[j:i]) / len(PPGs[j:i])

        frames_data[f] = {'SCL': SCL, 'PPG': PPG}

        frame_end_time = f * (1/fps)
        j = i
        f = f + 1

    return frames_data


